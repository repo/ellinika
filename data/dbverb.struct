-- This file is part of Ellinika
-- Copyright (C) 2004, 2005, 2007 Sergey Poznyakoff
--
-- Ellinika is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 3 of the License, or
-- (at your option) any later version.
--
-- Ellinika is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
set names utf8;

DROP TABLE IF EXISTS verbflect;
CREATE TABLE verbflect(
 ident int(32) not null,                        -- REL 8
 sing1 varchar(32),
 sing2 varchar(32),
 sing3 varchar(32),
 plur1 varchar(32),
 plur2 varchar(32),
 plur3 varchar(32),
 UNIQUE(ident)
);

DROP TABLE IF EXISTS conjugation;
CREATE TABLE conjugation(
 conj char(32),                                 -- REL 9
 voice enum('act','pas'),                -- Ενεργητηκή/Μεσοπαθητική
 mood enum('ind','sub','imp'),
 tense varchar(128),
 thema char(32),  -- enum('pres','aor','sub','synt'), -- Ενεστώτα, Αόριστου, υποτακτικής, synthetic
 suffix char(32),
 flect int(32),                                 -- REL 8
 accmap char(7),                         -- accent map
 particle char(2),                       -- NULL/θα/να  
 aux varchar(128),                              -- REL 10
 auxtense char(32),
 fold varchar(16),
 KEY (conj),
 KEY (voice),
 KEY (mood)
);

DROP TABLE IF EXISTS participle;
CREATE TABLE participle (
 conj char(32),                                  -- REL 9
 voice enum('act','pas'),                 -- Ενεργητηκή/Μεσοπαθητική
 tense char(32),
 thema char(32), -- enum('pres','aor','sub'), -- Ενεστώτα, Αόριστου, υποτακτικής
 suffix char(32), 
 flect char(32),
 aux varchar(128)
);

INSERT INTO verbflect VALUES
(0, NULL, NULL, NULL, NULL, NULL, NULL),
-- Συζυγία Α'
(1, "ω", "εις", "ει", "ουμε", "ετε", "ουν(ε)"),
(2, "α", "ες", "ε", "αμε", "ατε", "αν"),
(8, NULL, "ε", NULL, NULL, "ετε", NULL),
(9, NULL, "ε", NULL, NULL, "τε", NULL),
(11, "ομαι", "εσαι", "εται", "όμαστε", "εστε", "ονται"),
(12, "όμουν", "όσουν", "όταν", "όμαστε", "όσαστε", "ονταν"),
(15, "ώ", "είς", "εί", "ούμε", "είτε", "ούν(ε)"),
(17, NULL, "ου", NULL, NULL, NULL, NULL),
(18, NULL, NULL, NULL, NULL, "είτε", NULL),
-- Συζυγία Β΄ - α΄ τάξη
(20, "ώ", "άς", "ά","άμε","άτε","ούν(ε)"), 
(21, "άω", "άς", "άει","ούμε","άτε","ούν(ε)"), 
(25, NULL, "α", NULL, NULL, "άτε", NULL),
(28, "ιέμαι", "ιέσαι", "ιέται", "ιόμαστε", "ιέστε", "ιούνται"),
(29, "ιόμουν", "ιόσουν", "ιόταν", "ιόμαστε", "ιόσαστε", "ιόνταν"),
(30, NULL, "ιέσαι", NULL, NULL, "ιέστε", NULL),
(31, NULL, "ου", NULL, NULL, NULL, NULL),
(32, NULL, NULL, NULL, NULL, "είτε", NULL),
-- Συζυγία Β΄ - β΄ τάξη
(33, NULL, "είς", NULL, NULL, "είτε", NULL),
(34, "ούμαι", "είσαι", "είται", "ούμαστε", "είστε", "ούνται"),
(35, "ούμουν", "ούσουν", "ούvταν", "ούμαστε", "ούσαστε", "ούνταν"),
(36, NULL, "είσαι", NULL, NULL, "είστε", NULL)
;

INSERT INTO conjugation VALUES
-- Συζυγία Α'
--  Ενεργητηκή φωνή
--   Οριστική
("A", "act", "ind", "Ενεστώτας", 'pres', NULL, 1, "000000", NULL, NULL, NULL, NULL),
("A", "act", "ind", "Παρατατικός", 'pres', NULL, 2, "333333+", NULL, NULL, NULL, NULL),
("A", "act", "ind", "Μέλλοντας διαρκείας", 'synt', NULL, 1, "000000", "θα", NULL, "Ενεστώτας", NULL),
("A", "act", "ind", "Αόριστος", 'aor', NULL, 2, "333333+", NULL, NULL, NULL, NULL),
("A", "act", "ind", "Παρακείμενος", 'synt', NULL, 0, NULL, NULL, "έχω", "Ενεστώτας", NULL),
("A", "act", "ind", "Υπερσυντέλικος", 'synt', NULL, 0, NULL, NULL, "έχω", "Παρατατικός", NULL),
("A", "act", "ind", "Συντελεσμένος μέλλοντας", 'synt', NULL, 0, NULL, "θα", "έχω", "Ενεστώτας", NULL),
("A", "act", "ind", "Μέλλοντας στιγμιαίος", 'sub', NULL, 1, "000000", "θα", NULL, NULL, NULL),
--   Υποτακτική
("A", "act", "sub", "Ενεστώτας", 'synt', NULL, 1, "000000", "να", NULL, "Ενεστώτας", NULL),
("A", "act", "sub", "Αόριστος", 'sub', NULL, 1, "000000", "να", NULL, NULL, NULL),
("A", "act", "sub", "Παρακείμενος", 'synt', NULL, 0, NULL, "να", "έχω", "Ενεστώτας", NULL),
--   Προστακτική
("A", "act", "imp", "Ενεστώτας", 'pres', NULL, 8, "-3--3-", NULL, NULL, NULL, NULL),
("A", "act", "imp", "Αόριστος", 'sub', NULL, 9, "-3--0-", NULL, NULL, NULL, NULL),
("A", "act", "imp", "Παρακείμενος", 'synt', NULL, 0, "-0--0-", "να", "έχω", "Ενεστώτας", NULL),
-- Μεσοπαθητική φωνή
--  Οριστική
("A", "pas", "ind", "Ενεστώτας", 'pres', NULL, 11, "000300", NULL, NULL, NULL, NULL),
("A", "pas", "ind", "Παρατατικός", 'pres', NULL, 12, "222333", NULL, NULL, NULL, NULL),
("A", "pas", "ind", "Μέλλοντας διαρκείας", 'synt', NULL, 11, NULL, "θα", NULL, "Ενεστώτας", NULL),
("A", "pas", "ind", "Αόριστος", 'aor', "ηκ", 2, "333333", NULL, NULL, NULL, NULL),
("A", "pas", "ind", "Παρακείμενος", 'synt', NULL, 0, NULL, NULL, "έχω", "Ενεστώτας", NULL),
("A", "pas", "ind", "Υπερσυντέλικος", 'synt', NULL, 0, NULL, NULL, "έχω", "Παρατατικός", NULL),
("A", "pas", "ind", "Συντελεσμένος μέλλοντας", 'synt', NULL, 0, NULL, "θα", "έχω", "Ενεστώτας", NULL),
("A", "pas", "ind", "Μέλλοντας στιγμιαίος", 'aor', NULL, 15, "ffffff", "θα", NULL, NULL, NULL),
--   Υποτακτική
("A", "pas", "sub", "Ενεστώτας", 'synt', NULL, 11, NULL, "να", NULL, "Ενεστώτας", NULL),
("A", "pas", "sub", "Αόριστος", 'sub', NULL, 15, "ffffff", "να", NULL, NULL, NULL),
("A", "pas", "sub", "Παρακείμενος", 'synt', NULL, 0, NULL, "να", "έχω", "Ενεστώτας", NULL),
--   Προστακτική
-- ?synt??
("A", "pas", "imp", "Ενεστώτας", 'pres', NULL, 11, "-3--3-", "να", NULL, NULL, NULL),
("A", "pas", "imp", "Αόριστος", 'aor:act', NULL, 17, "-0----", NULL, NULL, NULL,  "imp-aor"),
("A", "pas", "imp", "Αόριστος", 'aor', NULL, 18, "----2-", NULL, NULL, NULL, "imp-aor"),
("A", "pas", "imp", "Παρακείμενος", 'synt', NULL, 0, "-0--0-", "να", "έχω", "Ενεστώτας", NULL),
-- Συζυγία Β΄ - α΄ τάξη
--  Ενεργητηκή φωνή
--   Οριστική
("B1", "act", "ind", "Ενεστώτας", 'pres', NULL, 20, "ffffff", NULL, NULL, NULL, NULL),
("B1", "act", "ind", "Ενεστώτας", 'pres', NULL, 21, "ffffff", NULL, NULL, NULL, NULL),
("B1", "act", "ind", "Παρατατικός", 'pres', "ούσ", 2, "ssssss", NULL, NULL, NULL, NULL),
("B1", "act", "ind", "Παρατατικός", 'pres', "αγ", 2, "333333", NULL, NULL, NULL, NULL),
("B1", "act", "ind", "Μέλλοντας διαρκείας", 'synt', NULL, 0, NULL, "θα", NULL, "Ενεστώτας", NULL),
-- ("B1", "act", "ind", "Μέλλοντας διαρκείας", 'pres', NULL, 21, "ffffff", "θα", NULL, NULL, NULL),
("B1", "act", "ind", "Αόριστος", 'aor', "ησ", 2, "333333", NULL, NULL, NULL, NULL),
("B1", "act", "ind", "Παρακείμενος", 'synt', NULL, 0, NULL, NULL, "έχω", "Ενεστώτας", NULL),
("B1", "act", "ind", "Υπερσυντέλικος", 'synt', NULL, 0, NULL, NULL, "έχω", "Παρατατικός", NULL),
("B1", "act", "ind", "Συντελεσμένος μέλλοντας", 'synt', NULL, 0, NULL, "θα", "έχω", "Ενεστώτας", NULL),
("B1", "act", "ind", "Μέλλοντας στιγμιαίος", 'aor', "ήσ", 1, "ssssss", "θα", NULL, NULL, NULL),
--   Υποτακτική
("B1", "act", "sub", "Ενεστώτας", 'synt', NULL, 0, NULL, "να", NULL, "Ενεστώτας", NULL),
("B1", "act", "sub", "Αόριστος", 'aor', "ήσ", 1, "ssssss", "να", NULL, NULL, NULL),
("B1", "act", "sub", "Παρακείμενος", 'synt', NULL, 0, NULL, "να", "έχω", "Ενεστώτας", NULL),
--   Προστακτική
("B1", "act", "imp", "Ενεστώτας", 'pres', NULL, 25, "-2--2-", NULL, NULL, NULL, NULL),
("B1", "act", "imp", "Αόριστος", 'aor', "ησ", 9, "-3--2-", NULL, NULL, NULL, NULL),
("B1", "act", "imp", "Παρακείμενος", 'synt', NULL, 0, "-0--0-", "να", "έχω", "Ενεστώτας", NULL),
--  Μεσοπαθητική φωνή
--   Οριστική
("B1", "pas", "ind", "Ενεστώτας", 'pres', NULL, 28, "ffffff", NULL, NULL, NULL, NULL),
("B1", "pas", "ind", "Παρατατικός", 'pres', NULL, 29, "ffffff", NULL, NULL, NULL, NULL),
("B1", "pas", "ind", "Μέλλοντας διαρκείας", 'synt', NULL, 0, NULL, "θα", NULL, "Ενεστώτας", NULL),
("B1", "pas", "ind", "Αόριστος", 'aor', "ηκ", 2, "333333", NULL, NULL, NULL, NULL),
("B1", "pas", "ind", "Παρακείμενος", 'synt', NULL, 0, NULL, NULL, "έχω", "Ενεστώτας", NULL),
("B1", "pas", "ind", "Υπερσυντέλικος", 'synt', NULL, 0, NULL, NULL, "έχω", "Παρατατικός", NULL),
("B1", "pas", "ind", "Συντελεσμένος μέλλοντας", 'synt', NULL, 0, NULL, "θα", "έχω", "Ενεστώτας", NULL),
("B1", "pas", "ind", "Μέλλοντας στιγμιαίος", 'aor', NULL, 15, "ffffff", "θα", NULL, NULL, NULL),
--   Υποτακτική
("B1", "pas", "sub", "Ενεστώτας", 'synt', NULL, 0, NULL, "να", NULL, "Ενεστώτας", NULL),
("B1", "pas", "sub", "Αόριστος", 'aor', NULL, 15, "ffffff", "να", NULL, NULL, NULL),
("B1", "pas", "sub", "Παρακείμενος", 'synt', NULL, 0, NULL, "να", "έχω", "Παρατατικός", NULL),
--   Προστακτική
("B1", "pas", "imp", "Ενεστώτας", 'pres', NULL, 30, "-f--f-", "να", NULL, NULL, NULL),

("B1", "pas", "imp", "Αόριστος", 'aor:act', "ησ", 31, "-s----", NULL, NULL, NULL, "imp-aor"),
("B1", "pas", "imp", "Αόριστος", 'aor', NULL, 32, "----2-", NULL, NULL, NULL, "imp-aor"),

("B1", "pas", "imp", "Παρακείμενος", 'synt', NULL, 0, "-0--0-", "να", "έχω", "Ενεστώτας", NULL),
-- Συζυγία Β΄ - β΄ τάξη
--  Ενεργητηκή φωνή
--   Οριστική
("B2", "act", "ind", "Ενεστώτας", 'pres', NULL, 15, "ffffff", NULL, NULL, NULL, NULL),
("B2", "act", "ind", "Παρατατικός", 'pres', "ούσ", 2, "ssssss", NULL, NULL, NULL, NULL),
("B2", "act", "ind", "Μέλλοντας διαρκείας", 'synt', NULL, 0, NULL, "θα", NULL, "Ενεστώτας", NULL),
("B2", "act", "ind", "Αόριστος", 'aor', "ησ", 2, "333333", NULL, NULL, NULL, NULL),
("B2", "act", "ind", "Παρακείμενος", 'synt', NULL, 0, NULL, NULL, "έχω", "Ενεστώτας", NULL),
("B2", "act", "ind", "Υπερσυντέλικος", 'synt', NULL, 0, NULL, NULL, "έχω", "Παρατατικός", NULL),
("B2", "act", "ind", "Συντελεσμένος μέλλοντας", 'synt', NULL, 0, NULL, "θα", "έχω", "Ενεστώτας", NULL),
("B2", "act", "ind", "Μέλλοντας στιγμιαίος", 'aor', "ήσ", 1, "ssssss", "θα", NULL, NULL, NULL),
--   Υποτακτική
("B2", "act", "sub", "Ενεστώτας", 'synt', NULL, 0, NULL, "να", NULL, "Ενεστώτας", NULL),
("B2", "act", "sub", "Αόριστος", 'aor', "ήσ", 1, "ssssss", "να", NULL, NULL, NULL),
("B2", "act", "sub", "Παρακείμενος", 'synt', NULL, 0, NULL, "να", "έχω", "Ενεστώτας", NULL),
--   Προστακτική
("B2", "act", "imp", "Ενεστώτας", 'pres', NULL, 15, "-f----", "να", NULL, NULL, "imp-enes"),
("B2", "act", "imp", "Ενεστώτας", 'pres', NULL, 15, "----f-", NULL, NULL, NULL, "imp-enes"),

("B2", "act", "imp", "Αόριστος", 'aor', "ησ", 9, "-3--3-", NULL, NULL, NULL, NULL),
("B2", "act", "imp", "Παρακείμενος", 'synt', NULL, 0, "-0--0-", "να", "έχω", "Ενεστώτας", NULL),
--  Μεσοπαθητική φωνή
--   Οριστική
("B2", "pas", "ind", "Ενεστώτας", 'pres', NULL, 34, "ffffff", NULL, NULL, NULL, NULL),
("B2", "pas", "ind", "Παρατατικός", 'pres', NULL, 35, "ffffff", NULL, NULL, NULL, NULL),
("B2", "pas", "ind", "Μέλλοντας διαρκείας", 'synt', NULL, 0, NULL, "θα", NULL, "Ενεστώτας", NULL),
("B2", "pas", "ind", "Αόριστος", 'aor', "ηκ", 2, "333333", NULL, NULL, NULL, NULL),
("B2", "pas", "ind", "Παρακείμενος", 'synt', NULL, 0, NULL, NULL, "έχω", "Ενεστώτας", NULL),
("B2", "pas", "ind", "Υπερσυντέλικος", 'synt', NULL, 0, NULL, NULL, "έχω", "Παρατατικός", NULL),
("B2", "pas", "ind", "Συντελεσμένος μέλλοντας", 'synt', NULL, 0, NULL, "θα", "έχω", "Ενεστώτας", NULL),
("B2", "pas", "ind", "Μέλλοντας στιγμιαίος", 'aor', NULL, 15, "ffffff", "θα", NULL, NULL, NULL),
--   Υποτακτική
("B2", "pas", "sub", "Ενεστώτας", 'synt', NULL, 0, NULL, "να", NULL, "Ενεστώτας", NULL),
("B2", "pas", "sub", "Αόριστος", 'aor', NULL, 15, "ffffff", "να", NULL, NULL, NULL),
("B2", "pas", "sub", "Παρακείμενος", 'synt', NULL, 0, NULL, "να", "έχω", "Παρατατικός", NULL),
--   Προστακτική
("B2", "pas", "imp", "Ενεστώτας", 'pres', NULL, 36, "-f--f-", "να", NULL, NULL, NULL),

("B2", "pas", "imp", "Αόριστος", 'aor:act', NULL, 31, "-2----", NULL, NULL, NULL, "imp-aor"),
("B2", "pas", "imp", "Αόριστος", 'aor', NULL, 32, "----2-", NULL, NULL, NULL, "imp-aor"),

("B2", "pas", "imp", "Παρακείμενος", 'synt', NULL, 0, "-0--0-", "να", "έχω", "Ενεστώτας", NULL),
-- Αποθετικά ρήματα
("A-depon", "act", "sub", "Αόριστος", 'sub', NULL, 1, "000000", "να", NULL, NULL, NULL),
--  Μεσοπαθητική φωνή
--   Οριστική
("A-depon", "pas", "ind", "Ενεστώτας", 'pres', NULL, 11, "000300", NULL, NULL, NULL, NULL),
("A-depon", "pas", "ind", "Παρατατικός", 'pres', NULL, 12, "222333", NULL, NULL, NULL, NULL),
("A-depon", "pas", "ind", "Μέλλοντας διαρκείας", 'synt', NULL, 0, NULL, "θα", NULL, "Ενεστώτας", NULL),
("A-depon", "pas", "ind", "Αόριστος", 'aor', NULL, 2, "000000", NULL, NULL, NULL, NULL),
("A-depon", "pas", "ind", "Παρακείμενος", 'synt', NULL, 0, NULL, NULL, "έχω", "Ενεστώτας", NULL),
("A-depon", "pas", "ind", "Υπερσυντέλικος", 'synt', NULL, 0, NULL, NULL, "έχω", "Παρατατικός", NULL),
("A-depon", "pas", "ind", "Συντελεσμένος μέλλοντας", 'synt', NULL, 0, NULL, "θα", "έχω", "Ενεστώτας", NULL),
("A-depon", "pas", "ind", "Μέλλοντας στιγμιαίος", 'sub', NULL, 15, "000000", "θα", NULL, NULL, NULL),
--   Υποτακτική
("A-depon", "pas", "sub", "Ενεστώτας", 'synt', NULL, 0, NULL, "να", NULL, "Ενεστώτας", NULL),
("A-depon", "pas", "sub", "Αόριστος", 'sub', NULL, 1, "000000", "να", NULL, NULL, NULL),
("A-depon", "pas", "sub", "Παρακείμενος", 'synt', NULL, 0, NULL, "να", "έχω", "Ενεστώτας", NULL),
--   Προστακτική -- FIXME
("A-depon", "pas", "imp", "Ενεστώτας", 'pres', NULL, 11, "-3--3-", "να", NULL, NULL, NULL),
("A-depon", "pas", "imp", "Αόριστος", 'aor', NULL, 17, "-0----", NULL, NULL, NULL, "imp-aor"),
("A-depon", "pas", "imp", "Αόριστος", 'aor', NULL, 18, "----2-", NULL, NULL, NULL, "imp-aor"),
("A-depon", "pas", "imp", "Παρακείμενος", 'synt', NULL, 0, "-0--0-", "να", "έχω", "Ενεστώτας", NULL)
;

INSERT INTO participle VALUES
-- Συζυγία Α'
--  Ενεργητηκή φωνή
("A", "act", "Ενεστώτας", 'pres', NULL, "οντας", NULL),
("A", "act", "Παρακείμενος", 'aor', NULL, "ει", "εχοντας"),
-- Μεσοπαθητική φωνή
("A", "pas", "Παρακείμενος", 'root', NULL, "μένος", NULL),
-- Συζυγία Β΄ - α΄ τάξη
--  Ενεργητηκή φωνή
("B1", "act", "Ενεστώτας", 'pres', NULL, "ώντας", NULL),
("B1", "act", "Παρακείμενος", 'aor', NULL, "ήσει", "εχοντας"),
--  Μεσοπαθητική φωνή
("B1", "pass", "Παρακείμενος", 'pres', "η", "μένος", NULL),
-- Συζυγία Β΄ - β΄ τάξη
--  Ενεργητηκή φωνή
("B2", "act", "Ενεστώτας", 'pres', NULL, "ώντας", NULL),
("B2", "act", "Παρακείμενος", 'aor', NULL, "ει", "εχοντας"),
--  Μεσοπαθητική φωνή
("B1", "pass", "Παρακείμενος", 'pres', "η", "μένος", NULL);

DROP TABLE IF EXISTS verbclass;
CREATE TABLE verbclass(
 verb varchar(128) collate 'utf8_bin',           -- REL 10
 conj char(32),                                 -- REL 9
 INDEX(verb),
 INDEX(conj)
);

DROP TABLE IF EXISTS verbtense;
CREATE TABLE verbtense(
 verb varchar(128) collate 'utf8_bin',           -- REL 10
 voice enum('act','pas'),              
 mood enum('ind','sub','imp'),
 tense varchar(128),
 property char(32),
 value varchar(128),
 INDEX(verb,voice,mood,tense)
);

DROP TABLE IF EXISTS individual_verb;
CREATE TABLE individual_verb(
 verb varchar(128) collate 'utf8_bin',
 voice enum('act','pas'),              
 mood enum('ind','sub','imp'),
 tense varchar(128),
 ident int(32),
 INDEX(verb,voice,mood,tense)
);

DROP TABLE IF EXISTS irregular_stem;
CREATE TABLE irregular_stem(
 verb varchar(128) collate 'utf8_bin',
 voice enum('act','pas'),                 -- Ενεργητηκή/Μεσοπαθητική
 thema char(32), -- enum('pres','aor','sub'),                 -- Αόριστος/Υποτακτική
 stem varchar(128)
);

