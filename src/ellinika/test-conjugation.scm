;;;; This file is part of Ellinika project.
;;;; Copyright (C) 2011 Sergey Poznyakoff
;;;;
;;;; Ellinika is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; Ellinika is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (ellinika test-conjugation))

(use-modules (srfi srfi-1)
	     (ellinika elmorph)
             (ellinika i18n)
	     (ellinika cgi)
	     (ellinika tenses)
	     (ellinika conjugator)
	     (ellinika sql))

(ellinika-cgi-init dict-template-file-name)
(ellinika:sql-connect ellinika-sql-connection)

(define-public (test-conjugation:tense verb voice mood tense)
  (for-each
   (lambda (result)
     (format #t "~A ~A/~A/~A: " verb
	     (ellinika-conjugation-term voice)
	     (ellinika-conjugation-term mood) tense)
     (let ((conj (conjugation:table result)))
       (cond
	((empty-conjugation? conj)
	 (display "#f"))
	(else
	 (let ((att (conjugation:attested result)))
	   (cond
	    ((not att)
	     (display "*"))
	    (else
	     (if (not (member 'class att))
		 (display "*"))
	     (if (not (member 'stem att))
		 (display "!"))))
	   (display conj)))))
     (newline))
   (conjugator verb voice mood tense))
  (gc))

(define-public (test-conjugation:voice voice verb)
  (for-each
   (lambda (mood-tenses)
     (let ((mood (car mood-tenses)))
       (for-each
	(lambda (tense)
	  (test-conjugation:tense verb voice mood tense))
	(cdr mood-tenses))))
   ellinika-tense-list))

(define-public (test-conjugation:verb verb)
  (test-conjugation:voice "act" verb)
  (test-conjugation:voice "pas" verb))

