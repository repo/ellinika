(load-extension "./libelmorph" "scm_init_ellinika_elmorph_module")

(for-each
 (lambda (root)
   (display (elstr-thema-aoristoy root))
   (newline))
 (list
  "χαν"
  "νιωθ"
  "διαβαζ"
  "πλεκ"
  "ανοιγ"
  "προσεχ"
  "διδασκ"
  "φτιαχν"
  "αλλαζ"
  "λειπ"
  "σκαβ"
  "βάφ"
  "δουλεύ"
  "παύ"
  "ζη"))
