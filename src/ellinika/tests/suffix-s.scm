(load-extension "./libelmorph" "scm_init_ellinika_elmorph_module")

(display
 (elstr-suffix? "παρακείμενος" "μενος"))
(newline)
(display
 (elstr-suffix? "παρακείμενος" "μεν"))
(newline)
(display
 (elstr-suffix? "παρακείμενος" "α" "οντας" "μενος"))
(newline)
