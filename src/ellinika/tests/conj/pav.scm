(use-modules ((ellinika test-conjugation)))

;(test-conjugation:verb "πάω")
(test-conjugation:tense "πάω" "act" "ind" "Ενεστώτας")
;(test-conjugation:tense "πάω" "act" "ind" "Μέλλοντας στιγμιαίος")
;(test-conjugation:tense "πάω" "act" "sub" "Ενεστώτας")
;(test-conjugation:tense "πάω" "act" "sub" "Αόριστος")
