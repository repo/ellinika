(load-extension "./libelmorph" "scm_init_ellinika_elmorph_module")

(let ((word (string->elstr "παρακείμενος")))
  (display (elstr-slice word 4 2))
  (newline)
  (elstr-slice! word 4 2)
  (display word)
  (newline))
