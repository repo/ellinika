(load-extension "./libelmorph" "scm_init_ellinika_elmorph_module")

(load "../elmorph-public.scm")

(let ((word (string->elstr "παρακείμενος")))
  (display (elstr->soundslike word))
  (newline))
