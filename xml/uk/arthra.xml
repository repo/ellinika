<!--

  Copyright (C) 2006 Sergey Poznyakoff

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.2
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover
  Texts.  A copy of the license is included in the file COPYING.FDL  -->

<PAGE PREFIX="arthra" HEADER="Артикль">
<PARA><DFN>Артикль</DFN> -- службова частина мови, яка окреслює
категорію визначеності / невизначеності іменника.
</PARA>

<SECTION>
<HEADER>Визначений артикль</HEADER>

<TABULAR ALTERNATE="1" ROWHEADING="std" COLHEADING="std">
 <TITLE>Відмінювання визначеного артикля</TITLE>
 <ROW>
  <ITEM>Відмінок</ITEM>
  <ITEM>м.р.</ITEM>
  <ITEM>ж.р.</ITEM>
  <ITEM>с.р.</ITEM>
 </ROW> 
 <ROW>
  <ITEM>Ονομαστική</ITEM>
  <ITEM>ο</ITEM>
  <ITEM>η</ITEM>
  <ITEM>το</ITEM>
 </ROW>
 <ROW>
  <ITEM>Γενική</ITEM>
  <ITEM>του</ITEM>
  <ITEM>της</ITEM>
  <ITEM>του</ITEM>
 </ROW>
 <ROW>
  <ITEM>Αιτιατική</ITEM>
  <ITEM>τον</ITEM>
  <ITEM>την</ITEM>
  <ITEM>το</ITEM>
 </ROW>

 <ROW>
  <ITEM>Ονομαστική</ITEM> 	
  <ITEM>οι</ITEM>
  <ITEM>οι</ITEM>
  <ITEM>τα</ITEM>
 </ROW>
 <ROW>
  <ITEM>Γενική</ITEM> 	
  <ITEM>των</ITEM>
  <ITEM>των</ITEM>
  <ITEM>των</ITEM>
 </ROW>
 <ROW>
  <ITEM>Αιτιατική</ITEM>
  <ITEM>τους</ITEM>
  <ITEM>τις</ITEM>
  <ITEM>τα</ITEM>
 </ROW>
</TABULAR>

<ANCHOR ID="oristiko_arthro" />
<PARA>Визначений артикль використовується у наступних випадках:</PARA>

<ENUMERATE>
<ITEM>Як правило, з власними назвами, назвами народів та географічних
місцевостей: <EXAMPLE>ο Γιάννης</EXAMPLE> <TRANS>Яніс</TRANS>,
<EXAMPLE>ο Έλληνας</EXAMPLE> <TRANS>грек</TRANS>, <EXAMPLE>οι
Αθήναι</EXAMPLE> <TRANS>Афіни</TRANS></ITEM> 

<ITEM>Завжди з вказівними займенниками, які виступають у ролі
означення: <EXAMPLE>αυτό το βιβλίο</EXAMPLE> <TRANS>ця
книга</TRANS>.  Якщо іменник є визначений вказівним займенником та
прикметником, артикль ставиться перед прикметником: <EXAMPLE>αυτή η ωραία
γυναίκα</EXAMPLE> <TRANS>ця красива жінка</TRANS>.</ITEM>

<ITEM>Після займенникових прикметників <DICTREF>όλος</DICTREF>,
<DICTREF>ολόκληρος</DICTREF>, <DICTREF>ολάκερος</DICTREF>, а також
після відносного займенника <DICTREF>οποίος</DICTREF>: <EXAMPLE>όλοι οι
άνθρωποι</EXAMPLE> <TRANS>всі люди</TRANS>, <EXAMPLE>τον οποίον
είδα</EXAMPLE> <TRANS>той, якого я бачив</TRANS>.</ITEM>

<ITEM>Перед іменниками, визначеними за допомогою енклітичного
присвійного займенника: <EXAMPLE>το βιβλίο μου</EXAMPLE> <TRANS>моя
книга</TRANS>, <EXAMPLE>το δικό μου βιβλίο</EXAMPLE> <TRANS>моя (власна)
книга</TRANS>.</ITEM>

<ITEM>Перед субстантивованою частиною мови: <EXAMPLE>ο
<DICTREF>ευτυχής</DICTREF></EXAMPLE> <TRANS>щасливчик</TRANS></ITEM>
</ENUMERATE>

<PARA>Звичайний порядок слів: артикль - означення - іменник,
наприклад: <EXAMPLE>το ψηλό δέντρο</EXAMPLE> <TRANS>високе
дерево</TRANS>. Тим не менш, означення може стояти і після іменника; в
такому випадку артикль з'являється двічі: <EXAMPLE>το δέντρο το
ψηλό</EXAMPLE>.</PARA>

<PARA>Якщо артикль відноситься до кількох пов'язаних контекстом
іменників, часто використовується спільний для них артикль:
<EXAMPLE>τα ξάρτια και πανιά</EXAMPLE> <TRANS>снасті та
вітрила</TRANS>.</PARA>

<PARA>Артикль не використовується:</PARA>

<ENUMERATE>
<ITEM>Перед іменною частиною складеного присудка: <EXAMPLE>είμαι γιατρός</EXAMPLE>
<TRANS>я лікар</TRANS>.</ITEM>

<ITEM>У заголовках, вивісках, оголошеннях і т.д.</ITEM>

<ITEM>В переліках: <EXAMPLE>σε κείνα τα μέρη πού βουνά και κάμποι
είναι ντυμένοι λευκή στολή</EXAMPLE> <TRANS>в тих краях, де гори і
рівнини вбрані у білий одяг</TRANS>.</ITEM>

<ITEM>У певних ідіоматичних виразах: <EXAMPLE>σηκώνει
κεφάλι</EXAMPLE> <TRANS>він підносить голову</TRANS>, <EXAMPLE>έχει καλό
αυτί</EXAMPLE> <TRANS>має добрий слух</TRANS>.</ITEM>

</ENUMERATE>

<PARA>Артикль може також бути використаний замість іменника, щоб
уникнути його повторювання в реченні:  <EXAMPLE>η κατοικία μας και η
των γειτόνων μας</EXAMPLE> <TRANS>наше помешкання і помешкання наших
сусідів</TRANS>. 
</PARA>

</SECTION>

<SECTION ID="aoristo_arthro">
<HEADER>Неозначений артикль</HEADER>
<TABULAR ALTERNATE="1" ROWHEADING="std" COLHEADING="std">
 <TITLE>Відмінювання неозначеного артикля</TITLE>
 <ROW>
  <ITEM>Відмінок</ITEM>
  <ITEM>м.р.</ITEM>
  <ITEM>ж.р.</ITEM>
  <ITEM>с.р.</ITEM>
 </ROW> 

  <ROW>
  <ITEM>Ονομαστική</ITEM>
  <ITEM>ένας</ITEM>
  <ITEM>μια</ITEM>
  <ITEM>ένα</ITEM>
 </ROW>

 <ROW>
  <ITEM>Γενική</ITEM>
  <ITEM>ενός</ITEM>
  <ITEM>μιας</ITEM>
  <ITEM>ενός</ITEM>
 </ROW>

 <ROW>
  <ITEM>Αιτιατική</ITEM>
  <ITEM>ένα(ν)</ITEM>
  <ITEM>μια</ITEM>
  <ITEM>ένα</ITEM>
 </ROW>

</TABULAR>
</SECTION>


</PAGE>

<!-- Local Variables: -->
<!-- mode: ellinika -->
<!-- buffer-file-coding-system: utf-8 -->
<!-- alternative-input-method: ukrainian-computer -->
<!-- alternative-dictionary: "ukrainian" -->
<!-- End: -->

