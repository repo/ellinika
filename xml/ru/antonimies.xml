<!--

  Copyright (C) 2004 Sergey Poznyakoff

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.2
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover
  Texts.  A copy of the license is included in the file COPYING.FDL  -->

<CHAPTER PREFIX="antonimies">
<PAGE HEADER="Местоимение">
<PARA>
Местоимение (αντωνημία) заменяет в предложении существительное,
прилагательное либо числительное.  В греческом языке существуют
следующие типы местоимений:
</PARA>

<ITEMIZE>
<ITEM>Личные (προσωπικές αντωνημίες)</ITEM>
<ITEM>Притяжательные (κτητικές αντωνημίες)</ITEM>
<ITEM>Указательные (δεικτικές αντωνημίες)</ITEM>
<ITEM>Относительные (αναφορικές αντωνημίες)</ITEM>
<ITEM>Вопросительные (ερωτηματικές αντωνημίες)</ITEM>
<ITEM>Неопределённые (αόριστες αντωνημίες)</ITEM>
</ITEMIZE>

</PAGE>

<PAGE HEADER="Личные местоимения">
<SECTION>
<HEADER>Безударные формы местоимений</HEADER>
<TABULAR ALTERNATE="1" ROWHEADING="std" COLHEADING="std">
 <ROW>
  <ITEM></ITEM>
  <ITEM>1-е лицо</ITEM>
  <ITEM>2-е лицо</ITEM>
  <ITEM>3-е лицо</ITEM>
 </ROW>

 <ROW>
  <ITEM>Ονομαστική</ITEM>
  <ITEM>εγώ</ITEM>
  <ITEM>εσύ</ITEM>
  <ITEM>αυτός,-ή,-ό</ITEM>
 </ROW>

 <ROW>
  <ITEM>Γενική</ITEM>
  <ITEM>μου</ITEM>
  <ITEM>σου</ITEM>
  <ITEM>του/της/του</ITEM>
 </ROW>

 <ROW>
  <ITEM>Αιτιατική</ITEM>
  <ITEM>με</ITEM>
  <ITEM>σε</ITEM>
  <ITEM>τον/την/το</ITEM>
 </ROW>

 <ROW>
  <ITEM>Ονομαστική</ITEM>
  <ITEM>εμείς</ITEM>
  <ITEM>εσείς</ITEM>
  <ITEM>αυτοί,-ές,-ά</ITEM>
 </ROW>

 <ROW>
  <ITEM>Γενική</ITEM>
  <ITEM>μας</ITEM>
  <ITEM>σας</ITEM>
  <ITEM>τους</ITEM>
 </ROW>

 <ROW>
  <ITEM>Αιτιατική</ITEM>
  <ITEM>μας</ITEM>
  <ITEM>σας</ITEM>
  <ITEM>τους/τις/τα</ITEM>
 </ROW>
</TABULAR>
</SECTION>
<SECTION>
<HEADER>Ударные формы местоимений</HEADER>
<TABULAR ALTERNATE="1" ROWHEADING="std" COLHEADING="std">
 <ROW>
  <ITEM></ITEM>
  <ITEM>1-е лицо</ITEM>
  <ITEM>2-е лицо</ITEM>
  <ITEM>3-е лицо</ITEM>
 </ROW>

 <ROW>
  <ITEM>Ονομαστική</ITEM>
  <ITEM>εγώ</ITEM>
  <ITEM>εσύ</ITEM>
  <ITEM>αυτός,-ή,-ό</ITEM>
 </ROW>

 <ROW>
  <ITEM>Γενική</ITEM>
  <ITEM>εμένα</ITEM>
  <ITEM>εσένα</ITEM>
  <ITEM>σ΄αυτόν,-ήν,-ό</ITEM>
 </ROW>

 <ROW>
  <ITEM>Αιτιατική</ITEM>
  <ITEM>εμένα</ITEM>
  <ITEM>εσένα</ITEM>
  <ITEM>αυτόν,-ήν,-ό</ITEM>
 </ROW>

 <ROW>
  <ITEM>Ονομαστική</ITEM>
  <ITEM>εμείς</ITEM>
  <ITEM>εσείς</ITEM>
  <ITEM>αυτοί,-ές,-ά</ITEM>
 </ROW>

 <ROW>
  <ITEM>Γενική</ITEM>
  <ITEM>εμάς</ITEM>
  <ITEM>εσάς</ITEM>
  <ITEM>σ΄αυτούς,-ές,-ά</ITEM>
 </ROW>

 <ROW>
  <ITEM>Αιτιατική</ITEM>
  <ITEM>εμάς</ITEM>
  <ITEM>εσάς</ITEM>
  <ITEM>αυτούς,-ές,-ά</ITEM>
 </ROW>

</TABULAR>
</SECTION>

<SECTION>
<HEADER>Использование местоимений в форме прямого и косвенного дополнения</HEADER>
<PARA>
Если к одному глаголу относятся два местоимения, заменяющие прямое и
косвенное дополнения, то они стоят перед глаголом в следующем порядке:
</PARA>

<EXAMPLE TYPE="PARA">
Косвенное дополнение + Прямое дополнение + Глагол
</EXAMPLE>

<PARA>
Например:
</PARA>

<TABULAR TYPE="EXAMPLE">
 <ROW>
  <EXAMPLE>(δεν) του το έδωσα.</EXAMPLE>
  <TRANS>Я ему это (не) дал.</TRANS>
 </ROW>
 <ROW>
  <EXAMPLE>(δεν) θα σου τον δώσω.</EXAMPLE>
  <TRANS>Я тебе это (не) дам.</TRANS>
 </ROW>
 <ROW>
  <EXAMPLE>να (μην) της την δώσεις</EXAMPLE>
  <TRANS>(Не) давай ей её.</TRANS>
 </ROW>
</TABULAR>

<PARA>В повелительных предложениях местоимения следуют за глаголом в
повелительном наклонении.</PARA>

<TABULAR TYPE="EXAMPLE">
 <ROW>
  <EXAMPLE>Ρώτησέ τον.</EXAMPLE>
  <TRANS>Спроси его.</TRANS>
 </ROW>
 <ROW>
  <EXAMPLE>Γράψτε τούς τα.</EXAMPLE>
  <TRANS>Напиши им их.</TRANS>
 </ROW>
</TABULAR>

<PARA>
При этом местоимение <SAMP>τις</SAMP> заменяется на
<SAMP>τες</SAMP>: <EXAMPLE>Φέρτε τις εφημερίδες. Φέρτε <EMPH>τες</EMPH>.</EXAMPLE>
</PARA>

<PARA>
Если местоимение третьего лица в винительном падеже (прямое
дополнение) следует за двусложным глаголом в повелительном наклонении
(<SAMP>δώσε</SAMP>, <SAMP>πάρε</SAMP>, <SAMP>φέρε</SAMP>), конечное
<FLECT>ε</FLECT> глагола, как правило, опускается: <EXAMPLE>Φέρε το
αμέσως. == Φέρ'το αμέσως. Πάρε τες από εδώ == Πάρ'τες από
εδώ.</EXAMPLE>
</PARA>   

<PARA>
В таком случае, если глагол управляет двумя местоимениями, они следуют
за ним в обратном порядке, т.е. сначала местоимение в винительном
падеже (прямое дополнение), затем местоимение в родительном падеже
(косвенное дополнение): <EXAMPLE>Φέρε μου το αμέσως. == Φέρ'το μου αμέσως.</EXAMPLE>
</PARA>

<PARA>
Местоимения в форме дополнения являются <XREF REF="enklitikoi">энклитиками</XREF>, поэтому если при их использовании
с глаголом образуются три безударных слога, необходима постановка
дополнительного ударения в соответсвии с <XREF REF="stress_change">общими правилами</XREF>:
</PARA>

<TABULAR BESTFIT="1" NOFRAME="1">
 <ROW>
  <ITEM>Διάβασε το βιβλίο.</ITEM>
  <ITEM>Διάβασέ το.</ITEM>
 </ROW>
 <ROW>
  <ITEM>Φέρτε την εφημερίδα στον παππού.</ITEM>
  <ITEM>Φέρτε τού την.</ITEM>
 </ROW>
</TABULAR>   
   
</SECTION>
</PAGE>

<PAGE HEADER="Притяжательные местоимения">
<PARA>
Притяжательные местоимения совпадают по форме с родительным падежом безударной
формы личных местоимений:

<TABULAR ALTERNATE="1" COLHEADING="std">
 <ROW>
  <ITEM>1-е лицо</ITEM>
  <ITEM>2-е лицо</ITEM>
  <ITEM>3-е лицо</ITEM>
 </ROW>

 <ROW>
  <ITEM>μου</ITEM>
  <ITEM>σου</ITEM>
  <ITEM>του/της/του</ITEM>
 </ROW>

 <ROW>
  <ITEM>μας</ITEM>
  <ITEM>σας</ITEM>
  <ITEM>τους</ITEM>
 </ROW>
</TABULAR>
</PARA>

<PARA>
Притяжательные местоимения всегда ставятся после существительного:

<EXAMPLE>Αυτό είναι το σπίτι μου.</EXAMPLE>
</PARA>

<PARA>
Ударные формы притяжательных местоимений образуются описательно, с
помощью прилагательного <DICTREF>δικός</DICTREF> и соответствующей
безударной формы:

<EXAMPLE>Αυτό το σπίτι είναι δικό μου.</EXAMPLE>
</PARA>

</PAGE>

<PAGE HEADER="Указательные местоимения">

<TABULAR ALTERNATE="1">
<TITLE><DICTREF>αυτός</DICTREF> -- этот</TITLE>
<ROW>
 <ITEM>αυτός</ITEM>
 <ITEM>αυτή</ITEM>
 <ITEM>αυτό</ITEM>
</ROW>
<ROW>
 <ITEM>αυτού</ITEM>
 <ITEM>αυτής</ITEM>
 <ITEM>αυτό</ITEM>
</ROW>
<ROW>
 <ITEM>αυτόν</ITEM>
 <ITEM>αυτήν</ITEM>
 <ITEM>αυτό</ITEM>
</ROW>
<ROW>
 <ITEM>αυτοί</ITEM>
 <ITEM>αυτές</ITEM>
 <ITEM>αυτά</ITEM>
</ROW>
<ROW>
 <ITEM>αυτών</ITEM>
 <ITEM>αυτών</ITEM>
 <ITEM>αυτών</ITEM>
</ROW>
<ROW>
 <ITEM>αυτούς</ITEM>
 <ITEM>αυτές</ITEM>
 <ITEM>αυτά</ITEM>
</ROW>
</TABULAR>

<TABULAR ALTERNATE="1">
<TITLE><DICTREF>εκείνος</DICTREF> -- тот</TITLE>
<ROW>
 <ITEM>εκείνος</ITEM>
 <ITEM>εκείνη</ITEM>
 <ITEM>εκείνο</ITEM>
</ROW>
<ROW>
 <ITEM>εκείνου</ITEM>
 <ITEM>εκείνης</ITEM>
 <ITEM>εκείνου</ITEM>
</ROW>
<ROW>
 <ITEM>εκείνον</ITEM>
 <ITEM>εκείνη</ITEM>
 <ITEM>εκείνο</ITEM>
</ROW>
<ROW>
 <ITEM>εκείνοι</ITEM>
 <ITEM>εκείνες</ITEM>
 <ITEM>εκείνα</ITEM>
</ROW>
<ROW>
 <ITEM>εκείνων</ITEM>
 <ITEM>εκείνων</ITEM>
 <ITEM>εκείνων</ITEM>
</ROW>
<ROW>
 <ITEM>εκείνους</ITEM>
 <ITEM>εκείνες</ITEM>
 <ITEM>εκείνα</ITEM>
</ROW>
</TABULAR>

<TABULAR ALTERNATE="1">
<TITLE><DICTREF>τέτοιος</DICTREF> -- такой, подобный</TITLE>
<ROW>
 <ITEM>τέτοιος</ITEM>
 <ITEM>τέτοια</ITEM>
 <ITEM>τέτοιο</ITEM>
</ROW>
<ROW>
 <ITEM>τέτοιου</ITEM>
 <ITEM>τέτοιας</ITEM>
 <ITEM>τέτοιου</ITEM>
</ROW>
<ROW>
 <ITEM>τέτοιον</ITEM>
 <ITEM>τέτοια</ITEM>
 <ITEM>τέτοιο</ITEM>
</ROW>
<ROW>
 <ITEM>τέτοιοι</ITEM>
 <ITEM>τέτοιες</ITEM>
 <ITEM>τέτοια</ITEM>
</ROW>
<ROW>
 <ITEM>τέτοιων</ITEM>
 <ITEM>τέτοιων</ITEM>
 <ITEM>τέτοιων</ITEM>
</ROW>
<ROW>
 <ITEM>τέτοιους</ITEM>
 <ITEM>τέτοιες</ITEM>
 <ITEM>τέτοια</ITEM>
</ROW>
</TABULAR>

<TABULAR ALTERNATE="1">
<TITLE><DICTREF>τόσος</DICTREF> -- такой</TITLE>
<ROW>
 <ITEM>τόσος</ITEM>
 <ITEM>τόση</ITEM>
 <ITEM>τόσο</ITEM>
</ROW>
<ROW>
 <ITEM>τόσου</ITEM>
 <ITEM>τόσης</ITEM>
 <ITEM>τόσου</ITEM>
</ROW>
<ROW>
 <ITEM>τόσον</ITEM>
 <ITEM>τόση</ITEM>
 <ITEM>τόσο</ITEM>
</ROW>
<ROW>
 <ITEM>τόσοι</ITEM>
 <ITEM>τόσες</ITEM>
 <ITEM>τόσα</ITEM>
</ROW>
<ROW>
 <ITEM>τόσων</ITEM>
 <ITEM>τόσων</ITEM>
 <ITEM>τόσων</ITEM>
</ROW>
<ROW>
 <ITEM>τόσους</ITEM>
 <ITEM>τόσες</ITEM>
 <ITEM>τόσα</ITEM>
</ROW>
</TABULAR>

</PAGE>

<PAGE HEADER="Относительные местоимения">
<TABULAR ALTERNATE="1">
<TITLE><DICTREF>οποίος</DICTREF> -- который, какой</TITLE>
<ROW>
 <ITEM>ο οποίος</ITEM>
 <ITEM>η οποία</ITEM>
 <ITEM>το οποίο</ITEM>
</ROW>
<ROW>
 <ITEM>του οποίου</ITEM>
 <ITEM>της οποίας</ITEM>
 <ITEM>του οποίου</ITEM>
</ROW>
<ROW>
 <ITEM>τον οποίον</ITEM>
 <ITEM>την οποία</ITEM>
 <ITEM>το οποίο</ITEM>
</ROW>
<ROW>
 <ITEM>οι οποίοι</ITEM>
 <ITEM>οι οποίες</ITEM>
 <ITEM>οι οποία</ITEM>
</ROW>
<ROW>
 <ITEM>των οποίων</ITEM>
 <ITEM>των οποίων</ITEM>
 <ITEM>των οποίων</ITEM>
</ROW>
<ROW>
 <ITEM>τους οποίος</ITEM>
 <ITEM>τις οποίες</ITEM>
 <ITEM>τα οποία</ITEM>
</ROW>
</TABULAR>

<TABULAR ALTERNATE="1">
<TITLE><DICTREF>όποιος</DICTREF> -- тот кто, всякий</TITLE>
<ROW>
 <ITEM>όποιος</ITEM>
 <ITEM>όποια</ITEM>
 <ITEM>όποιο</ITEM>
</ROW>
<ROW>
 <ITEM>όποιου</ITEM>
 <ITEM>όποιας</ITEM>
 <ITEM>όποιου</ITEM>
</ROW>
<ROW>
 <ITEM>όποιον</ITEM>
 <ITEM>όποια</ITEM>
 <ITEM>όποιο</ITEM>
</ROW>
<ROW>
 <ITEM>όποιοι</ITEM>
 <ITEM>όποιες</ITEM>
 <ITEM>όποια</ITEM>
</ROW>
<ROW>
 <ITEM>όποιων</ITEM>
 <ITEM>όποιων</ITEM>
 <ITEM>όποιων</ITEM>
</ROW>
<ROW>
 <ITEM>όποιος</ITEM>
 <ITEM>όποιες</ITEM>
 <ITEM>όποια</ITEM>
</ROW>
</TABULAR>

<TABULAR ALTERNATE="1">
<TITLE><DICTREF>όσος</DICTREF> -- столько, столько сколько</TITLE>
<ROW>
 <ITEM>όσος</ITEM>
 <ITEM>όση</ITEM>
 <ITEM>όσο</ITEM>
</ROW>
<ROW>
 <ITEM>όσου</ITEM>
 <ITEM>όσης</ITEM>
 <ITEM>όσου</ITEM>
</ROW>
<ROW>
 <ITEM>όσον</ITEM>
 <ITEM>όση</ITEM>
 <ITEM>όσο</ITEM>
</ROW>
<ROW>
 <ITEM>όσοι</ITEM>
 <ITEM>όσες</ITEM>
 <ITEM>όσα</ITEM>
</ROW>
<ROW>
 <ITEM>όσων</ITEM>
 <ITEM>όσων</ITEM>
 <ITEM>όσων</ITEM>
</ROW>
<ROW>
 <ITEM>όσους</ITEM>
 <ITEM>όσες</ITEM>
 <ITEM>όσα</ITEM>
</ROW>
</TABULAR>

<SECTION>
<HEADER>Несклоняемые местоимения</HEADER>
<TABULAR NOFRAME="1">
<ROW>
 <ITEM>τι</ITEM>
 <ITEM>сколько, что за... </ITEM>
</ROW>
<ROW>
 <ITEM>που</ITEM>
 <ITEM>который, кто, что</ITEM>
</ROW>
</TABULAR>
</SECTION>

</PAGE>

<PAGE HEADER="Вопросительные местоимения">
<PARA>
<TABULAR ALTERNATE="1">
<TITLE><DICTREF>ποιος</DICTREF> -- кто?, какой?</TITLE>
<ROW>
 <ITEM>ποιος</ITEM>
 <ITEM>ποια</ITEM>
 <ITEM>ποιο</ITEM>
</ROW>
<ROW>
 <ITEM>ποιου/τίνος</ITEM>
 <ITEM>ποιας</ITEM>
 <ITEM>ποιου/τίνος</ITEM>
</ROW>
<ROW>
 <ITEM>ποιον</ITEM>
 <ITEM>ποια</ITEM>
 <ITEM>ποιο</ITEM>
</ROW>
<ROW>
 <ITEM>ποιοι</ITEM>
 <ITEM>ποιες</ITEM>
 <ITEM>ποια</ITEM>
</ROW>
<ROW>
<ITEM SPAN="3">ποιων/ποιανών/τίνων</ITEM>
</ROW>
<ROW>
 <ITEM>ποιους</ITEM>
 <ITEM>ποιες</ITEM>
 <ITEM>ποια</ITEM>
</ROW>
</TABULAR>
</PARA>

<PARA>
<TABULAR ALTERNATE="1">
<TITLE><DICTREF>πόσος</DICTREF> -- сколько?</TITLE>
<ROW>
 <ITEM>πόσος</ITEM>
 <ITEM>πόση</ITEM>
 <ITEM>πόσο</ITEM>
</ROW>
<ROW>
 <ITEM>πόσου</ITEM>
 <ITEM>πόσης</ITEM>
 <ITEM>πόσου</ITEM>
</ROW>
<ROW>
 <ITEM>πόσον</ITEM>
 <ITEM>πόση</ITEM>
 <ITEM>πόσο</ITEM>
</ROW>
<ROW>
 <ITEM>πόσοι</ITEM>
 <ITEM>πόσες</ITEM>
 <ITEM>πόσα</ITEM>
</ROW>
<ROW>
 <ITEM>πόσων</ITEM>
 <ITEM>πόσων</ITEM>
 <ITEM>πόσων</ITEM>
</ROW>
<ROW>
 <ITEM>πόσους</ITEM>
 <ITEM>πόσες</ITEM>
 <ITEM>πόσα</ITEM>
</ROW>
</TABULAR>
</PARA>

<SECTION>
<HEADER>Несклоняемые местоимения</HEADER>
<TABULAR NOFRAME="1">
<ROW>
 <ITEM>τί</ITEM>
 <ITEM>что?</ITEM>
</ROW>
<ROW>
 <ITEM>πού</ITEM>
 <ITEM>где? κуда?</ITEM>
</ROW>
</TABULAR>
</SECTION>

</PAGE>

<PAGE HEADER="Неопределённые местоимения">
<PARA>
Неопределённые местоимения подразделяются на <DFN>склоняемые</DFN> и <DFN>несклоняемые</DFN>.
В свою очередь, склоняемые местоимения подразделяются на имеющие только единственное число
(<DFN>singularia tantum</DFN>), имеющие только множественное число (<DFN>pluralia tantum</DFN>)
и склоняемые в обоих числах.
</PARA>

<SECTION>
<HEADER>Употребление <DICTREF>κανένας</DICTREF> и <DICTREF>τίποτα</DICTREF></HEADER>
<PARA>
В утвердительных и вопросительных предложениях <SAMP>κανένας</SAMP> означает <TRANS>кто-либо</TRANS>,
<TRANS>некто</TRANS>, <SAMP>τίποτα</SAMP> означает <TRANS>какой-нибудь</TRANS>.
</PARA>
<PARA>
Примеры:
</PARA>
<TABULAR TYPE="EXAMPLE">
 <ROW>
  <EXAMPLE>Αν μου τηλεφωνήσει <EMPH>κανείς</EMPH>, να του πεις να ξαναπάρει το απόγευμα.</EXAMPLE>
  <TRANS>Если мне <EMPH>кто-нибудь</EMPH> позвонит, скажи ему, чтобы перезвонил завтра.</TRANS>
 </ROW>
 <ROW>
  <EXAMPLE>Έχεις <EMPH>κανένα</EMPH> βιβλίο να μου δώσεις;</EXAMPLE>
  <TRANS>Есть у тебя <EMPH>какая-нибудь</EMPH> книга, которую ты мог бы мне дать?</TRANS>
 </ROW>
 <ROW>
  <EXAMPLE>Αν μάθεις <EMPH>τίποτα</EMPH> να μου το πεις.</EXAMPLE>
  <TRANS>Если <EMPH>что-то</EMPH> узнаешь, скажи мне.</TRANS>
 </ROW>
</TABULAR>

<PARA>
В отрицательных предложениях <SAMP>κανένας</SAMP> означает <TRANS>никто</TRANS>, а <SAMP>τίποτα</SAMP> -- <TRANS>ничего</TRANS>:
</PARA>
<TABULAR TYPE="EXAMPLE">
 <ROW>
  <EXAMPLE><EMPH>Κανείς</EMPH> δεν είδε τον δολοφόνο.</EXAMPLE>
  <TRANS><EMPH>Никто</EMPH> не видел убийцу.</TRANS>
 </ROW>
 <ROW>
  <EXAMPLE>Δεν έχω <EMPH>τίποτε</EMPH> να σου πω.</EXAMPLE>
  <TRANS>Мне <EMPH>нечего</EMPH> тебе сказать</TRANS>
 </ROW>
 <ROW>
  <EXAMPLE><PARA>-- Ποιος ήρθε σήμερα;</PARA><PARA>-- <EMPH>Κανείς.</EMPH></PARA></EXAMPLE>
  <TRANS><PARA>-- Кто пришел сегодня?</PARA><PARA>-- <EMPH>Никто.</EMPH></PARA></TRANS>
 </ROW>
 <ROW>
  <EXAMPLE><PARA>-- Τι έγινε;</PARA><PARA>-- <EMPH>Τίποτε</EMPH></PARA></EXAMPLE>
  <TRANS><PARA>-- Что случилось?</PARA><PARA>-- <EMPH>Ничего</EMPH></PARA></TRANS>
 </ROW>
</TABULAR>
</SECTION>

<SECTION>
<HEADER>Употребление <DICTREF>κάποιος</DICTREF></HEADER>
<PARA>
Местоимение <DICTREF>κάποιος</DICTREF> обозначает в предложении неопределённое либо неизвестное
лицо или предмет и переводится как <TRANS>кто-то</TRANS>, <TRANS>кто-либо</TRANS>, либо с помощью
прилагательного <TRANS>какой-то</TRANS>, <TRANS>кое-какой</TRANS>:
</PARA>
<TABULAR TYPE="EXAMPLE">
 <ROW>
 <EXAMPLE>Ήρθε σήμερα <EMPH>κάποιος</EMPH> στο γραφείο μου;</EXAMPLE>
 <TRANS><EMPH>Кто-нибудь</EMPH> сегодня приходил ко мне в контору?</TRANS>
 </ROW>
 <ROW>
 <EXAMPLE>Έχω <EMPH>κάποια</EMPH> έπιπλα και θέλω να τα πουλήσω.</EXAMPLE>
 <TRANS>У меня есть <EMPH>кое-какая</EMPH> мебель и я хочу её продать.</TRANS>
 </ROW>
</TABULAR>
</SECTION>

<SECTION>
<HEADER>Употребление <DICTREF>μερικοί</DICTREF></HEADER>
<PARA>
Местоимение <DICTREF>μερικοί</DICTREF> обозначает небольшое количество предметов или лиц. Оно
употребляется только в неопределённом контексте и переводится <TRANS>некоторые</TRANS>, <TRANS>
немногие</TRANS>, <TRANS>несколько</TRANS>.
</PARA>

<TABULAR TYPE="EXAMPLE">
 <ROW>
 <EXAMPLE>Ήρθαν <EMPH>μερικοί</EMPH> νέοι στη διάλεξη.</EXAMPLE>
 <TRANS>На лекцию пришло <EMPH>несколько</EMPH> молодых людей</TRANS>
 </ROW>
 <ROW>
 <EXAMPLE>Υπήρχαν <EMPH>μερικές</EMPH> καρέκλες σ΄εκείνο το δωμάτιο.</EXAMPLE>
 <TRANS>В той комнате было <EMPH>несколько</EMPH> стульев</TRANS>
 </ROW>
</TABULAR>
</SECTION>

<SECTION>
<HEADER>Употребление <DICTREF>άλλος</DICTREF></HEADER>
<TABULAR TYPE="EXAMPLE">
 <ROW>
 <EXAMPLE>Ο Γιάννης έγινε <EMPH>άλλος</EMPH> άνθρωπος.</EXAMPLE>
 <TRANS>Янис стал <EMPH>другим</EMPH> человеком.</TRANS>
 </ROW>
 <ROW>
 <EXAMPLE>Πού είναι τα <EMPH>άλλα</EMPH> παιδιά;</EXAMPLE>
 <TRANS>Где <EMPH>остальные</EMPH> дети?</TRANS>
 </ROW>
 <ROW>
 <EXAMPLE>Εγώ κράτησα όσα βλέπεις. Τα <EMPH>άλλα</EMPH> τα έδωσα στον αδελφό μου.</EXAMPLE>
 <TRANS>Я взял сколько ты видишь. <EMPH>Остальное</EMPH> отдал моему брату.</TRANS>
 </ROW>
</TABULAR>
</SECTION>

<SECTION>
<HEADER>Употребление <DICTREF>κάθε</DICTREF></HEADER>
<TABULAR TYPE="EXAMPLE">
 <ROW>
  <EXAMPLE><EMPH>Κάθε</EMPH> άνθρωπος έχει και τα ελαττώματά του.</EXAMPLE>
  <TRANS>У <EMPH>каждого</EMPH> есть свои недостатки.</TRANS>
 </ROW>
 <ROW>
  <EXAMPLE><EMPH>Κάθε</EMPH> Κυριακή πηγαίναμε βόλτα στο Φάληρο.</EXAMPLE>
  <TRANS><EMPH>Каждое</EMPH> воскресенье мы ходили гулять в Фалиро.</TRANS>
 </ROW>
</TABULAR>
</SECTION>

<SECTION>
<HEADER>Употребление <DICTREF>κάτι</DICTREF></HEADER>
<PARA>
При использовании вместо существительного, несклоняемое местоимение
<DICTREF>κάτι</DICTREF> означает неизвестный или неопределённый предмет
или лицо и переводится <TRANS>что-то</TRANS>:
</PARA>
<TABULAR TYPE="EXAMPLE">
 <ROW>
  <EXAMPLE>Ο Χρίστος βρήκε <EMPH>κάτι</EMPH> στον δρόμο.</EXAMPLE>
  <TRANS>Христос <EMPH>что-то</EMPH> нашёл на дороге.</TRANS>
 </ROW>
 <ROW>
  <EXAMPLE>Ο Νίκος είδε <EMPH>κάτι</EMPH> στον ουρανό.</EXAMPLE>
  <TRANS>Никос заметил <EMPH>что-то</EMPH> в небе.</TRANS>
 </ROW>
</TABULAR>
<PARA>
Когда <SAMP>κάτι</SAMP> используется в качестве прилагательного,
т.е. относится к какому-либо существительному, независимо от его рода
и падежа, оно обозначает неопределённость этого существительного и
переводится в таком случае как <TRANS>какой-то</TRANS>,
<TRANS>какой-нибудь</TRANS>, <TRANS>некоторый</TRANS>, <TRANS>один</TRANS>:
</PARA>
<TABULAR TYPE="EXAMPLE">
 <ROW>
  <EXAMPLE>Ο Γιώργος διαβάζει <EMPH>κάτι</EMPH> παλιά βιβλία.</EXAMPLE>
  <TRANS>Йоргос читает <EMPH>какие-то</EMPH> старые книги.</TRANS>
 </ROW>
 <ROW>
  <EXAMPLE>Ήρθαν <EMPH>κάτι</EMPH> ξένοι και γύρευαν τον διευθυντή.</EXAMPLE>
  <TRANS>Пришли <EMPH>какие-то</EMPH> незнакомцы и разыскивали директора.</TRANS>
 </ROW>
 <ROW>
  <EXAMPLE>Δεν σε επισκέφτηκα, γιατί είχα <EMPH>κάτι</EMPH> δουλειές να κάνω.</EXAMPLE>
  <TRANS>Я к тебе не зашёл, потому что мне нужно было сделать <EMPH>кое-какие</EMPH> дела.</TRANS>
 </ROW>
</TABULAR>
</SECTION>

<SECTION>
<HEADER>Использование артикля</HEADER>
<PARA>
Артикль никогда не используется с неопределёнными местоимениями <SAMP>κάποιος</SAMP>, <SAMP>κανένας</SAMP>,
<SAMP>μερικοί</SAMP>. 
</PARA>
<PARA>
Местоимения <SAMP>άλλος</SAMP> и <SAMP>κάθε</SAMP> могут использоваться как с артиклем так и без него.
</PARA>
</SECTION>

<SECTION>
<HEADER>Склонение неопределённых местоимений</HEADER>
<PARA>
Местоимение <DICTREF>ένας</DICTREF> (некий, какой-то), склоняется так же,
как <XREF REF="aoristo_arthro">неопределённый артикль</XREF>.
</PARA>
<TABULAR ALTERNATE="1">
<TITLE><DICTREF>κανένας</DICTREF></TITLE>
<ROW>
 <ITEM>κανένας (κανείς)</ITEM>
 <ITEM>καμιά (καμία)</ITEM>
 <ITEM>κανένα</ITEM>
</ROW>
<ROW>
 <ITEM>κανένος</ITEM>
 <ITEM>καμιάς</ITEM>
 <ITEM>κανενός</ITEM>
</ROW>
<ROW>
 <ITEM>κανένα(ν)</ITEM>
 <ITEM>καμιά</ITEM>
 <ITEM>κανένα</ITEM>
</ROW>
</TABULAR>

<TABULAR SPACE="2" ALTERNATE="1">
<TITLE><DICTREF>κάποιος</DICTREF></TITLE>
<ROW>
 <ITEM>κάποιος</ITEM>
 <ITEM>κάποια</ITEM>
 <ITEM>κάποιο</ITEM>
</ROW>
<ROW>
 <ITEM>κάποιου</ITEM>
 <ITEM>κάποιας</ITEM>
 <ITEM>κάποιου</ITEM>
</ROW>
<ROW>
 <ITEM>κάποιον</ITEM>
 <ITEM>κάποια</ITEM>
 <ITEM>κάποιο</ITEM>
</ROW>
<ROW>
 <ITEM>κάποιοι</ITEM>
 <ITEM>κάποιες</ITEM>
 <ITEM>κάποια</ITEM>
</ROW>
<ROW>
 <ITEM>κάποιων</ITEM>
 <ITEM>κάποιων</ITEM>
 <ITEM>κάποιων</ITEM>
</ROW>
<ROW>
 <ITEM>κάποιους</ITEM>
 <ITEM>κάποιες</ITEM>
 <ITEM>κάποια</ITEM>
</ROW>
</TABULAR>

<TABULAR SPACE="2" ALTERNATE="1">
<TITLE><DICTREF>μερικοί</DICTREF></TITLE>
<ROW>
 <ITEM>μερικοί</ITEM>
 <ITEM>μερικές</ITEM>
 <ITEM>μερικά</ITEM>
</ROW>
<ROW>
 <ITEM>μερικών</ITEM>
 <ITEM>μερικών</ITEM>
 <ITEM>μερικών</ITEM>
</ROW>
<ROW>
 <ITEM>μερικούς</ITEM>
 <ITEM>μερικές</ITEM>
 <ITEM>μερικά</ITEM>
</ROW>
</TABULAR>
</SECTION>

<SECTION>
<HEADER>Несклоняемые местоимения</HEADER>

<TABULAR NOFRAME="1">
<ROW>
 <ITEM>κάτι</ITEM>
 <ITEM>что-либо, что-то; какой-то</ITEM>
</ROW>
<ROW>
 <ITEM>τίποτε (τίποτα)</ITEM>
 <ITEM>что-либо, что-то</ITEM>
</ROW>
<ROW>
 <ITEM>κάθε</ITEM>
 <ITEM>каждый</ITEM>
</ROW>
</TABULAR>
</SECTION>

</PAGE>

</CHAPTER>

<!-- Local Variables: -->
<!-- mode: ellinika -->
<!-- buffer-file-coding-system: utf-8 -->
<!-- alternative-input-method: cyrillic-jcuken -->
<!-- alternative-dictionary: "russian" -->
<!-- End: -->
