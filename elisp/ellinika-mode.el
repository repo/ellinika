;;; ellinika-dict-mode.el --- major mode for editing Ellinika dictionary files

;; Authors: 2004 Sergey Poznyakoff
;; Version:  1.0
;; Keywords: Ellinika, greek
;; $Id$

;; This file is part of Ellinika.
;; Copyright (C) 2004, 2007 Sergey Poznyakoff.

;; Ellinika is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; Ellinika is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defun greek-input (arg)
  (interactive "p")
  (set-input-method
   (if (and (boundp 'greek-input-method) greek-input-method)
       greek-input-method
       'greek)))

(defun ancient-greek-input (arg)
  (interactive "p")
  (set-input-method
   (if (and (boundp 'ancient-greek-input-method) ancient-greek-input-method)
       ancient-greek-input-method
     'greek-ibycus4)))
  
(defun alt-input (arg)
  (interactive "p")
  (if (and (boundp 'alternative-input-method) alternative-input-method)
      (set-input-method alternative-input-method)
    (deactivate-input-method)))

(set-language-environment 'utf-8)


;;;;

(defcustom ellinika-close-tag-hook nil
  "Hook function run upon closing of a tag")

(defun ellinika-set-close-tag-hook (hook)
  (setq ellinika-close-tag-hook hook))

(defun ellinika-find-open-tag-internal ()
  (catch 'loop
    (let ((tag-list nil))
      (while (search-backward-regexp "<\\(/?[^ >]+\\)\\(\\s +[^>]*\\)?>"
				     nil t)
	(let ((tag (buffer-substring
		    (match-beginning 1)
		    (match-end 1))))
	  (cond
	   ((char-equal (string-to-char tag) ?\/)
	    (setq tag-list (cons (substring tag 1)
				 tag-list)))
	   ((looking-at "<!"))
	   ((looking-at "<[^ >]+\\(\\s +[^>]*\\)?/>"))
	   ((string-equal (car tag-list) tag)
	    (setq tag-list (cdr tag-list)))
	   (t
	    (throw 'loop tag))))))))

(defun ellinika-find-open-tag (&optional move-point)
  (if move-point
      (ellinika-find-open-tag-internal)
    (save-excursion
      (ellinika-find-open-tag-internal))))

(defun ellinika-close-tag (arg)
  (interactive "p")
  (let ((tag (ellinika-find-open-tag)))
    (cond
     (tag
      (insert (concat "</" tag ">"))
      (and ellinika-close-tag-hook (funcall ellinika-close-tag-hook)))
     (t
      (message "No open tags")))))

(defun ellinika-insert-include (arg)
  (interactive "p")
  (beginning-of-line)
  (insert "<INCLUDE FILE=\"\" />")
  (backward-char 4)
  (inactivate-input-method))

(defcustom alternative-input-method nil
  "Defines input-method for non-greek text in this buffer.")

(defcustom alternative-dictionary nil
  "Defines alternative (non-greek) spell-checking dictionary for this buffer.")

(defun ellinika-run-ispell (dict)
  (let ((ispell-skip-html t)
	(ispell-local-dictionary dict))
    (ispell)))

(defun ellinika-ispell-greek (arg)
  (interactive "p")
  (ellinika-run-ispell "greek"))

(defun ellinika-ispell-alt (arg)
  (interactive "p")
  (ellinika-run-ispell (if alternative-dictionary
			      alternative-dictionary
			 (error "Alternative dictionary not defined"))))
  
(defun ellinika-ispell (arg)
  (interactive "p")
  (ellinika-run-ispell  (if (> arg 0)
			    "greek"
			  (if alternative-dictionary
			      alternative-dictionary
			    (error "Alternative dictionary not defined")))))

(defun ellinika-electric-obrace (arg)
  (interactive "p")
  (inactivate-input-method)
  (self-insert-command arg))

(defun ellinika-electric-cbrace (arg)
  (interactive "p")
  (self-insert-command arg)
  (and ellinika-close-tag-hook (funcall ellinika-close-tag-hook)))


;;;###autoload
(define-derived-mode ellinika-mode sgml-mode "Ellinika"
  "Base major mode for editing Ellinika XML sources. All major modes
are derived from this one.

  Key bindings:
\\{ellinika-mode-map}"

  (make-variable-buffer-local 'alternative-input-method)
  (make-variable-buffer-local 'greek-input-method)
  (make-variable-buffer-local 'ancient-greek-input-method)
  (make-variable-buffer-local 'alternative-dictionary)
  (make-variable-buffer-local 'ellinika-close-tag-hook)
  
  (define-key ellinika-mode-map "\C-c\C-i" 'ellinika-insert-include)  
  (define-key ellinika-mode-map "\C-c\C-g" 'ellinika-ispell-greek)
  (define-key ellinika-mode-map "\C-c\C-s" 'ellinika-ispell-alt)

  (define-key ellinika-mode-map "\M-g" 'greek-input)
  (define-key ellinika-mode-map "\C-c\M-g" 'ancient-greek-input)
  (define-key ellinika-mode-map "\M-r" 'alt-input)
  (define-key ellinika-mode-map "<" 'ellinika-electric-obrace)
  (define-key ellinika-mode-map ">" 'ellinika-electric-cbrace)
  (define-key ellinika-mode-map "\C-c>" 'ellinika-close-tag))

(provide 'ellinika-mode)

;;;; End of ellinika-mode.el

